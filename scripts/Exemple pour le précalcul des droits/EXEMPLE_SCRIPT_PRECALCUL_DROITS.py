#Script python permettant de précalculer les droits pour l'accès aux données d'une WebMap

#importer les modules
from orionpy.orioncore.Orion import Orion
if __name__ == "__main__":
    
    ##### Propriétés à changer avant d'executer le script ####

    # Login administrateur 
    username = "login admin aob"
    # Mot de passe adminstrateur 
    password = "mot de passe"
    # Url du serveur 
    url_machine = "https://monserveur.fr"
    # Nom du webadaptor du portail
    portal = "portal"

    ##########################################################

    # Se connecter au serveur aOB en récupérant une instance de la classe Orion.
    orion = Orion(username, password, url_machine, portal)
    
    # Calcul des droits sur les cartes, indiquez l'ID de l'item dans le Portail
    orion.webmap_permissions_calculator.calculate_webmap_permissions("ID de l'item 1")
    orion.webmap_permissions_calculator.calculate_webmap_permissions("ID de l'item 2")
    orion.webmap_permissions_calculator.calculate_webmap_permissions("ID de l'item 3")

  

	
	
	
	
