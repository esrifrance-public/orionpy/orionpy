# Exemple de scripts Python pour OrionPy

Les scripts Python disponibles dans ce répertoire permettent de maintenir à jour les informations associées aux unités d'organisation d'arcOpole Builder.
Ils sont fournis à titre d'exemple et peuvent être modifiés en fonction de vos besoin.

##  1. <a name='Sommaire'></a>Sommaire

<!-- vscode-markdown-toc -->
* 1. [Sommaire](#Sommaire)
* 2. [Remerciements](#Remerciements)
* 3. [Participer](#Participer)
* 4. [Glossaire](#Glossaire)
* 5. [Préparation de l'environnement Python](#PrparationdelenvironnementPython)
	* 5.1. [Répertoire projet](#Rpertoireprojet)
	* 5.2. [Via l'environnement built-in Python](#Vialenvironnementbuilt-inPython)
	* 5.3. [Via l'environnement conda](#Vialenvironnementconda)
	* 5.4. [Via l'environnement conda cloné issu d'ArcGIS Pro](#VialenvironnementcondaclonissudArcGISPro)
* 6. [Utiliser les samples](#Utiliserlessamples)
	* 6.1. [Lancer un script via l'environnement built-in Python](#Lancerunscriptvialenvironnementbuilt-inPython)
	* 6.2. [Lancer un script via l'environnement conda](#Lancerunscriptvialenvironnementconda)
	* 6.3. [Lancer un script via l'environnement conda cloné issu d'ArcGIS Pro](#LancerunscriptvialenvironnementcondaclonissudArcGISPro)

##  2. <a name='Remerciements'></a>Remerciements 

L'équipe arcOpole tient à remercie la CA de Val Parisis qui a initié les premiers scripts et a bien voulu les transmettre pour relecture et enrichissement.

##  3. <a name='Participer'></a>Participer 

Vous aussi, vous pouvez allonger cette liste en fonction de vos besoins et en faire profiter la communauté arcOpole en prenant contact avec nous (arcopole@esrifrance.fr) ou sur GitLab : https://gitlab.com/esrifrance-public/orionpy/orionpy/-/tree/master/scripts

##  4. <a name='Glossaire'></a>Glossaire

- FDU = Filtre Dépendant de l'Utilisateur
- OU = _Organization Unit_ (fr: Unité d'organisation)

##  5. <a name='PrparationdelenvironnementPython'></a>Préparation de l'environnement Python

Plusieurs approches sont possibles. Choisir qu'une seule des options suivantes :

1. via l'environnement built-in `venv` de Python
2. via l'environnement `conda`
3. via l'environnement `conda` cloné issu d'ArcGIS Pro

###  5.1. <a name='Rpertoireprojet'></a>Répertoire projet

Ouvrir un poste de travail et se créer un répertoire projet.

Par exemple :

```text
C:\projet\monprojet
```

###  5.2. <a name='Vialenvironnementbuilt-inPython'></a>Via l'environnement built-in Python

Installer une version de Python 3.7 ou + depuis le site de Python : <https://www.python.org/downloads/>

Supposons que l'installation se soit effectuée dans le répertoire suivant :

```text
C:\App\python
```

Depuis une invite de commande windows (Démarrer > Invite de commande), créer l'environnement virtuel Python via la commande suivante :

```bat
C:\Users\mon_utilisateur > C:\App\python\python.exe -m venv C:\projet\monprojet\.venv
```

Un répertoire `.venv` va être créé dans `C:\projet\monprojet`.

Toujours charger l'environnement virtuel Python :

```bat
C:\Users\mon_utilisateur > C:\projet\monprojet\.venv\Scripts\activate.bat
(.venv) C:\Users\mon_utilisateur > 
```

Vérifier le lancement de Python

```bat
(.venv) C:\Users\mon_utilisateur > python
Python 3.9.1 (tags/v3.9.1:1e5d33e, Dec  7 2020, 17:08:21) [MSC v.1927 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Quitter la console Python

```bat
>>> exit()
(.venv) C:\Users\mon_utilisateur >
```

Installer la bibliothèque `orionpy`:

```bat
(.venv) C:\Users\mon_utilisateur > python -m pip install orionpy
```

Vérifier l'installation de la bibliothèque `orionpy`

```bat
(.venv) C:\Users\mon_utilisateur > python
Python 3.9.1 (tags/v3.9.1:1e5d33e, Dec  7 2020, 17:08:21) [MSC v.1927 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import orionpy
>>>
```

Aucune erreur ne doit être remontée. Quitter en saisissant `exit()`.

Quitter l'environnement virtuel python

```bat
(.venv) C:\Users\mon_utilisateur > deactivate
C:\Users\mon_utilisateur >
```

###  5.3. <a name='Vialenvironnementconda'></a>Via l'environnement conda

Installer Miniconda3 depuis le site : <https://docs.conda.io/en/latest/miniconda.html>

Supposons que l'installation se soit effectuée dans le répertoire suivant :

```text
C:\App\python\Miniconda3
```

Intialiser `conda` pour votre terminal préféré. Ici pour powershell :

```powershell
PS > conda init powershell
```

Vérifier l'installation de conda

```powershell
PS > conda env list
# conda environments:
#
base                  *  C:\App\python\Miniconda3
```

Créer un nouvel environnement virtuel `conda` dans le répertoire projet

```powershell
PS > conda create --prefix 'C:\projet\monprojet\env'
```

Toujours charger l'environnement conda Python :

```powershell
C:\Users\mon_utilisateur > conda activate C:\projet\monprojet\env
(C:\projet\monprojet\env) C:\Users\mon_utilisateur >
```

Vérifier le lancement de Python

```powershell
(C:\projet\monprojet\env) C:\Users\mon_utilisateur > python
Python 3.7.9 [MSC v.1922 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Quitter la console Python

```powershell
>>> exit()
(C:\projet\monprojet\env) C:\Users\mon_utilisateur >
```

Installer la bibliothèque `orionpy`:

```powershell
(C:\projet\monprojet\env) C:\Users\mon_utilisateur > python -m pip install orionpy
```

Vérifier l'installation de la bibliothèque `orionpy`

```powershell
(C:\projet\monprojet\env) C:\Users\mon_utilisateur > python
Python 3.7.9 [MSC v.1922 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import orionpy
>>>
```

Aucune erreur ne doit être remontée. Quitter en saisissant `exit()`.

Quitter l'environnement virtuel python

```powershell
(C:\projet\monprojet\env) C:\Users\mon_utilisateur > conda deactivate
C:\Users\mon_utilisateur >
```

###  5.4. <a name='VialenvironnementcondaclonissudArcGISPro'></a>Via l'environnement conda cloné issu d'ArcGIS Pro

Cloner l'environnement depuis l'interface d'ArcGIS Pro dans `Reglages > Python > Gérer les environnements` puis se positionner sur la ligne contenant `arcgispro-py3` et cliquer sur le bouton `cloner` représenté par deux carrés imbriqués.

Dans la pop-up, saisir (ou se déplacer) jusque dans le répertoire projet (C:\projet\monprojet), et laisser le nom par défaut (`arcgispro-py3-clone`)

Le clone peut prendre quelques minutes.

Vérifier le lancement de Python depuis l'environnement clonné

```powershell
C:\Users\mon_utilisateur > C:\projet\monprojet\arcgispro-py3-clone\python.exe
Python 3.7.9 [MSC v.1922 64 bit (AMD64)] :: Anaconda, Inc. on win32

Warning:
This Python interpreter is in a conda environment, but the environment has
not been activated.  Libraries may fail to load.  To activate this environment
please see https://conda.io/activation

Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Quitter la console Python

```powershell
>>> exit()
C:\Users\mon_utilisateur >
```

Installer la bibliothèque `orionpy`:

```powershell
C:\Users\mon_utilisateur > C:\projet\monprojet\arcgispro-py3-clone\python.exe -m pip install orionpy
```

Vérifier l'installation de la bibliothèque `orionpy`

```powershell
C:\Users\mon_utilisateur > C:\projet\monprojet\arcgispro-py3-clone\python.exe
Python 3.7.9 [MSC v.1922 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import orionpy
>>>
```

Aucune erreur ne doit être remontée. Quitter en saisissant `exit()`.

---

##  6. <a name='Utiliserlessamples'></a>Utiliser les samples

Les scripts disponibles sont fournis à titre d'exemple. Ils peuvent (et très souvent) doivent être ajustés à votre système ArcGIS.

Tous les scripts disposent

- d'une section `Propriétés` en début de script où les paramètres de connexion à votre Portal for ArcGIS sont obligatoires
- d'une section `Fonction` contenant l'execution du script

Que font les scripts ?

|Script|Fonctionnalité|
|---|---|
|CREER_FDU_UNITES_ORGANISATION.py|Création d'un FDU pour la gestion des OU|
|AJOUTER_UNITES_ORGANISATION.py|Ajouter des valeurs dans le FDU de gestion des OU|
|MAJ_UNITES_ORGANISATION.py|Mettre à jour des valeurs du FDU de gestion des OU|
|REMPLACER_TOUTES_UNITES_ORGANISATION.py|Remplacer toutes les valeurs du FDU de gestion des OU par des nouvelles|
|LISTER_FDU_UNITES_ORGANISATION.py|Afficher la liste des FDU|
|NOUVEAU_UTILISATEUR.py|Affecter une OU à un utilisateur|
|UTILISATEUR_CHANGE_UNITES_ORGANISATION.py|Changer l'OU d'un utilisateur|
|SUPPRIMER_VALEUR_UNITES_ORGANISATION_SUR_UTILISATEUR.py|Supprimer l'OU d'un utilisateur|
|RECUPERER_LISTE_UTILISATEURS_CSV.py|Exporter au format CSV tous les utilisateurs et leur OU affectée|
|MAJ_LISTE_UTILISATEURS_CSV.py|Importer un CSV contenant les utilisateurs et leur OU affectée (annule et remplace l'existant)|
|WEBSEMINAIRE_SuiviConnexions_Script_CreationUnitesOrganisation.py|Exemple utilisé lors du webséminaire sur le suivi des connexions|

_Note : Penser à éditer chacun des scripts avant de les lancer._

###  6.1. <a name='Lancerunscriptvialenvironnementbuilt-inPython'></a>Lancer un script via l'environnement built-in Python

Charger l'environnement virtuel `venv`

```bat
C:\Users\mon_utilisateur > C:\projet\monprojet\.venv\Scripts\activate.bat
(.venv) C:\Users\mon_utilisateur >
```

Exécuter le script Python souhaité, ici le script `CREER_FDU_UNITES_ORGANISATION.py`

```bat
(.venv) C:\Users\mon_utilisateur > C:\projet\monprojet\CREER_FDU_UNITES_ORGANISATION.py
...
```

###  6.2. <a name='Lancerunscriptvialenvironnementconda'></a>Lancer un script via l'environnement conda

Charger l'environnement virtuel `conda`

```powershell
C:\Users\mon_utilisateur > conda activate C:\projet\monprojet\env
(C:\projet\monprojet\env) C:\Users\mon_utilisateur >
```

Exécuter le script Python souhaité, ici le script `CREER_FDU_UNITES_ORGANISATION.py`

```powershell
(C:\projet\monprojet\env) C:\Users\mon_utilisateur > C:\projet\monprojet\CREER_FDU_UNITES_ORGANISATION.py
...
```

###  6.3. <a name='LancerunscriptvialenvironnementcondaclonissudArcGISPro'></a>Lancer un script via l'environnement conda cloné issu d'ArcGIS Pro

Exécuter le script Python souhaité, ici le script `CREER_FDU_UNITES_ORGANISATION.py`

```powershell
C:\Users\mon_utilisateur > C:\projet\monprojet\arcgispro-py3-clone\python.exe C:\projet\monprojet\CREER_FDU_UNITES_ORGANISATION.py
...
```
