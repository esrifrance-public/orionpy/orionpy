# Notes on project compilation

OrionPy is written in Python 3. Please refer to README.md file to install work environment.

# Publish OrionPy

To publish OrionPy on pypi, several steps must be accomplished.

Version number is stored in the **VERSION file**.
It is formatted as : `YY.MM.DD[rcX]`
> `rcX` is optional and in case of a _release candidate_.

## Useful links

Sources of information on how to publish a python project, writing the setup.py, etc... :

* https://packaging.python.org/tutorials/packaging-projects/
* https://packaging.python.org/tutorials/distributing-packages/
* https://github.com/pypa/sampleproject/blob/master/setup.py
* https://pypi.org/
* https://pypi.org/project/orionpy/ (where orionpy is published)

## Preparation
Ensure we have everything ready (setup.py, README.md, LICENSE, VERSION, …)

All module that are required to be installed are in the **publish-requirements.txt** file.\
You should be able to install it with the (_untested_) command :
```
python -m pip install --user --upgrade -r publish-requirements.txt
```

Ensure we have the latest version of setuptools and wheel :
```
python -m pip install --user --upgrade setuptools wheel
```

Generate an API Token https://pypi.org/help/#apitoken.\
Place the API Token in a file in HOME/.pypirc such as :
```
[pypi]
username = __token__
password = <the token value, including the `pypi-` prefix>
```
> An other solution is to manually write the username and API Token when uploading with twine.

Ensures that twine is installed and up to date :
```
python -m pip install --user --upgrade twine
```

## Execution

Once every necessary module was installed and our machine was configured,
we can execute the following commands to publish.

First, we need to ensure that the **dist** folder is empty. If not, remove its content.

> A good thing to do is also to regenerate the documentation (see DEV.md)

Then, from the folder where **setup.py** is :
```
python setup.py sdist bdist_wheel
```
Ensure that two files were created in our **dist** folder : A `.tar.gz` and a `.whl`.

Before upload, we can run the following command to check our deposit :
```
python -m twine check dist/*
```

Run `twine` to upload the built **dist** folder on pypi.org :
```
python -m twine upload dist/*
```

> The dist folder should be cleaned after each uploading.
> Otherwise, twine will try to upload every version.
>
> The option --repository testpypi can be used if we want to upload on testpypi.

# Jenkins configuration

## ds.esrifrance.js/jenkins Installation

> **IMPORTANT** : A lot of things have changed on Jenkins.
> Hence, this documentation might not be up to date.

This project is compiled on Jenkins with the job :
https://ds.esrifrance.fr/jenkins/job/Produit/job/orion-py/

To launch this job, we have to install python3 and pip on the process machine (ci.meudon.int).

To connect on ci.meudon.int please refer to the note describe in redmine. (http://vmaosredmine/projects/nextgen/wiki/Maintenance_de_cimeudonint)

On the server, we have executed the next command :
sudo apt install python3
sudo apt install python3-pip

sudo pip3 install virtualenv
sudo pip3 install nose

## Job configuration

The OrionPy Jenkins job is configure to connect on the git repository
It's define in source code management part
https://git.esrifrance.fr/aco/orionpy.git

For the build process, we configure 2 steps

### Build shell

The build shell allows to execute the python process a virtual environment.

```python
# Create virtualenv and install necessary packages
virtualenv --no-site-packages venv
. venv/bin/activate

pip install -r requirements.txt

nosetests --with-coverage --with-xunit --cover-package=src --cover-xml
```

### Generate documentation

The documentation is generated with Sphinx.

To generate the documentation use the next command 

```bash
pip install -r dev-requirements.txt
```

```
cd docs
./make html
```

### Execute SonarQube Scanner

After test, we execute SonarQube scanner with the next configuration :

```
# required metadata
sonar.projectKey=produits:aob:orionpy
sonar.projectName=OrionPy
sonar.projectVersion=1.0


# path to source directories (required)
sonar.sources=src

# path to test source directories (optional)
sonar.tests=tests

sonar.language=py

# Résultat des tests unitaires
sonar.python.xunit.reportPath=nosetests.xml

# Compre rendu du Code coverage
sonar.python.coverage.reportPath=coverage.xml
```

It uses Esri Sonar define in Jenkins.
The results are available here : 
http://ci.meudon.int:9000/dashboard?id=orionpy%3Aproject
