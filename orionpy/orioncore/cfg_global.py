# coding=utf-8
"""
Module containing global variables
"""

# =============================================================================
# CONSTANTS
# =============================================================================

# Boolean saying if the ArcGIS installation system is federated or not.
is_federated = True
